// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

// Package synovpn manages the VPN client on the Synology NAS, it contains the ability
// to write to the VPN client configuration file and read from the configuration
// file written by the NAS.
// The main functionality enables the ability to control the state of the VPN.
// Debug errors can be found in tail /var/log/messages.
package synovpn

import (
	"os"
	"os/exec"
	"time"
)

type Status int

const (
	Unknown Status = iota
	Started
	Stopped
	Starting
	Stopping
	Restarting
)

var statuses = [...]string{
	"Unknown",
	"Started",
	"Stopped",
	"Starting",
	"Stopping",
	"Restarting",
}

// String returns the English name of the Status
func (s Status) String() string { return statuses[s] }

type VPNService struct {
	LastKnownIP   string
	ServiceStatus Status
}

func (vs *VPNService) Restart() error {
	vs.Stop()

	switch vs.ServiceStatus {
	case Stopped:
		break
	case Stopping:
		time.Sleep(2 * time.Second)
	case Started:

	}
	vs.Start()
	return nil
}

func (vs *VPNService) Start() error {
	var c *exec.Cmd
	//echo 1 > /usr/syno/etc/synovpnclient/vpnc_connecting
	c = exec.Command("synovpnc reconnect --protocol=pptp --name=myVPN --retry=5")
	c.Stdout = os.Stdout
	err := c.Run()
	if err != nil {
		return err
	}
	return nil
}

func (vs *VPNService) Stop() error {

	vs.ServiceStatus = Stopping
	var c *exec.Cmd
	c = exec.Command("synovpnc kill_client", "--protocol=pptp", "--name=myVPN") // "/var/run/ovpn_client.pid"
	c.Stdout = os.Stdout
	err := c.Run()
	if err != nil {
		return err
	}

	//	while vs.currentStatus() == started {
	//		time.Sleep(2 * time.Second)
	//	}
	vs.ServiceStatus = Stopped
	return nil
}

func (vs *VPNService) currentStatus() (Status, error) {

	var c *exec.Cmd
	c = exec.Command("synovpnc", "--get", "--name=OctaneVPN")
	c.Stdout = os.Stdout
	err := c.Run()
	if err != nil {
		return Unknown, err
	}
	return Started, nil
}
