// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

package synovpn

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

type SynoVPNConfig struct {
	ClientID string
	Protocol string
	Name     string
	Server   string
}

// getSynoConfig gets the Octane VPN server and VPN client ID from the Synology ovpnclient.conf config file.
func GetSynoVPNConfig(configPath string) (*SynoVPNConfig, error) {

	svc := new(SynoVPNConfig)

	file, err := os.Open(configPath + "/ovpnclient.conf") // For read access.
	if err != nil {
		return nil, fmt.Errorf("unable to open ovpnclient.conf, make sure you use the Synology NAS UI to setup the VPN client first. Error: %v", err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		if strings.HasPrefix(scanner.Text(), "[o") { //line will be in the format of "[oNNNNNNNNNN]" where N = 0-9.
			svc.ClientID = scanner.Text()[2 : len(scanner.Text())-1]
		}
		if strings.HasPrefix(scanner.Text(), "remote") { // this line is in the format of "remote=octaneserver.octanevpn.com".
			svc.Server = strings.TrimPrefix(scanner.Text(), "remote=")
		}
	}

	var emsg = "unable to obtain the %s from the ovpnclient.conf file produced by the Synology web UI, please re-run the Synology web UI to fix this."
	if len(svc.ClientID) == 0 { //not checking validity, just happy if we have something.
		//TODO: add validity checking.
		return nil, fmt.Errorf(emsg + "VPN client ID")
	}
	if len(svc.Server) == 0 { // once again i don't care if it is valid
		//TODO: add validity checking.
		return nil, fmt.Errorf(emsg + "Octane VPN server")
	}
	return svc, nil
}
