// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

package synovpn_test

import (
	"testing"

	"github.com/icsoft/SynoOctane/synovpn"
)

func TestWriteConfig(t *testing.T) {
	_, err := synovpn.WriteConfig("gw1.test.octanevpn.com", "1234567890", "..")
	if err != nil {
		t.Error("Error writing config file")
	}
}
