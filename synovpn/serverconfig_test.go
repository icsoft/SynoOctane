// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

package synovpn_test

import (
	"testing"

	"github.com/icsoft/SynoOctane/synovpn"
)

func TestGetSynoVPNConfig(t *testing.T) {
	ret, err := synovpn.GetSynoVPNConfig("..")
	if err != nil {
		t.Error("Error reading config file")
	}
	if ret.ClientID != "1234567890" {
		t.Error("Invalid read of config file, expected 1234567890 got: ", ret.ClientID)
	}
}
