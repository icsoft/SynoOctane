// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

// Package synolog is a wrapper for accessing the proprietary Synology logger
// found in /usr/syno/bin/synologset1, it has 3 log levels and will only work
// on the Synology NAS.
package synolog

import (
	"fmt"
	"os"
	"os/exec"
)

// The three log levels, Warning, Information and Error
type LogLevel int

const (
	LogWarning LogLevel = iota
	LogInformation
	LogError
)

var logLevels = [...]string{
	"warn",
	"info",
	"err",
}

// String returns the English name of the Log Level
func (l LogLevel) String() string { return logLevels[l] }

// SynoLog is the main function to write to the Synology logger, it checks
// for existance of the executable first and fails silently if it is not found
// as it may be running on a different system.
// Log events are found in /usr/syno/synosdk/texts/enu/events
func SynoLog(lvl LogLevel, msg string) error {
	var c *exec.Cmd
	c = exec.Command("/usr/syno/bin/synologset1", "sys", lvl.String(), "0x11100000", fmt.Sprintf("\"%s\"", msg))

	// we may not be on the Synology NAS, so don't error if it's not there.
	if _, err := os.Stat(c.Path); os.IsNotExist(err) {
		return nil
	}

	c.Stdout = os.Stdout
	err := c.Run()
	if err != nil {
		return err
	}
	return nil
}
