// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

package synolog_test

import (
	"testing"

	"github.com/icsoft/SynoOctane/synolog"
)

// TestSynlog only works correctly on the Synology NAS
func TestSynolog(t *testing.T) {
	err := synolog.SynoLog(synolog.LogInformation, "synolog test")
	if err != nil {
		t.Error("Error returned from log ", err)
	}
}
