// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

// This package provides network utilities not provided by the standard package
package net

import (
	"errors"
	"io/ioutil"
	"net"
	"net/http"
	"strings"
)

func ExternalHazIP() (net.IP, error) {
	resp, err := http.Get("http://icanhazip.com/")
	if err != nil {
		return nil, errors.New("No responce from icanhzip server")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var ip = net.ParseIP(string(body[:len(body)-1]))
	return ip, nil
}

func ExternalDynIP() (net.IP, error) {
	resp, err := http.Get("http://checkip.dyndns.org/")
	if err != nil {
		return nil, errors.New("No responce from dyndns.org server")
	}
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)

	var ip net.IP
	s := string(body)
	s = s[strings.IndexRune(s, ':')+2:]
	ip = net.ParseIP(s[0:strings.IndexRune(s, '<')])
	return ip, nil
}

type IfaceIPs []IfaceIP

type IfaceIP struct {
	Iface string
	IP    net.IP
}

func NetworkIP() (IfaceIPs, error) {
	ifaces, err := net.Interfaces()
	if err != nil {
		return nil, err
	}

	var i IfaceIP
	var retval IfaceIPs

	for _, iface := range ifaces {
		if iface.Flags&net.FlagUp == 0 {
			continue // interface down
		}
		if iface.Flags&net.FlagLoopback != 0 {
			continue // loopback interface
		}
		addrs, err := iface.Addrs()
		if err != nil {
			return nil, err
		}

		for _, addr := range addrs {
			var ip net.IP
			switch v := addr.(type) {
			case *net.IPNet:
				ip = v.IP
			case *net.IPAddr:
				ip = v.IP
			}
			if ip == nil || ip.IsLoopback() {
				continue
			}
			ip = ip.To4()
			if ip == nil {
				continue // not an ipv4 address
			}
			//retval += iface.Name + ": " + ip.String() + "\n"
			i.Iface = iface.Name
			i.IP = ip
			retval = append(retval, i)
		}
	}
	if len(retval) == 0 {
		return nil, errors.New("no network interfaces with IPv4 addresses found, are you connected to a network?")
	}
	return retval, nil
}
