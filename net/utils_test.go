// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

package net_test

import (
	"testing"

	"github.com/icsoft/SynoOctane/net"
)

func TestExternalHazIP(t *testing.T) {
	_, err := net.ExternalHazIP()
	if err != nil {
		t.Error("icnhazip.com call returned an error ", err)
	}
}

func TestExternalDynIP(t *testing.T) {
	_, err := net.ExternalDynIP()
	if err != nil {
		t.Error("dyndns.com call returned an error ", err)
	}
}

func TestNetworkIP(t *testing.T) {
	_, err := net.NetworkIP()
	if err != nil {
		t.Error("error retrieving network IP ", err)
	}
}
