// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

// Package octanevpn lets the user select a server from the list of OctaneVPN
// servers from the command line.
// The servers are stored in a CSV string constant, extracted from the
// OctaneVPN website.
package octanevpn

import (
	"bytes"
	"encoding/csv"
	"fmt"
	"io"
	"math/rand"
	"strings"
	"time"
)

type FilterType int

const (
	Country FilterType = iota
	City
	Server
)

var filterTypes = [...]string{
	"Country",
	"City",
	"Server",
}

type OctaneData struct {
	filter      string
	filterType  FilterType
	initialised bool
}

func NewOctaneData() *OctaneData {
	var retval = new(OctaneData)
	retval.filterType = Country
	serverData = getServerData()
	retval.initialised = true
	return retval
}

// String returns the English name of the filter type
func (f FilterType) String() string { return filterTypes[f] }

func (od *OctaneData) SetFilterType(ft FilterType) {
	od.filterType = ft
	if ft == Country {
		od.filter = ""
		if !od.initialised {
			serverData = getServerData()
			od.initialised = true
		}
	}
}

func (od *OctaneData) SetFilter(filter string) {
	od.filter = filter
}

func (od *OctaneData) Data() ([]string, error) {
	if !od.initialised {
		return nil, fmt.Errorf("filter type not set, set the filter type or use \"New\" before trying to retrieve the data.")
	}
	if od.filterType != Country && od.filter == "" {
		return nil, fmt.Errorf("filter not set, set the filter using \"SetFilter\" to retrieve the data")
	}

	if serverData == nil {
		serverData = getServerData()
	}

	switch od.filterType {
	case Country:
		data, err := getCountries()
		if err != nil {
			return nil, err
		}
		return data, nil
	case City:
		data, err := getCities(od.filter)
		if err != nil {
			return nil, err
		}
		return data, nil
	case Server:
		data, err := getServers(od.filter)
		if err != nil {
			return nil, err
		}
		return data, nil
	}
	return nil, fmt.Errorf("invalid filter type")
}

func (od *OctaneData) String() string { return fmt.Sprintf("Filter: %s", od.filter) }

var serverData []byte

// getServerData gets all the OctaneVPN servers from a constant so no other
// files need to be shipped with this executable.
func getServerData() []byte {
	return []byte(octaneVPNServers)
}

func GetRandomServer() (string, error) {
	var s []string
	serverData = getServerData()

	r := csv.NewReader(bytes.NewReader(serverData))

	//we want all the servers first
	for {
		record, err := r.Read()
		if err == io.EOF {
			if len(s) == 0 {
				return "", fmt.Errorf("Server not found")
			}
			break
		}
		if err != nil {
			return "", fmt.Errorf("error whilst parsing list of servers. Error: %s", err)
		}
		s = append(s, strings.Fields(record[2])...)
	}

	rand.Seed(time.Now().UnixNano())
	retval := s[rand.Intn(len(s))]
	return retval, nil
}

// GetCountries gets a list of all countries supported by Octane VPN
func getCountries() ([]string, error) {
	var s []string

	r := csv.NewReader(bytes.NewReader(serverData))

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("error whilst parsing country data. Error: %s", err)
		}
		if len(record[0]) > 0 {
			s = append(s, record[0])
		}
	}
	return s, nil
}

// GetCities retrieves a list of cities from a given country name.
func getCities(countryName string) ([]string, error) {
	var s []string
	var foundCountry = false

	r := csv.NewReader(bytes.NewReader(serverData))

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("error whilst parsing city data for %s. Error: %s", countryName, err)
		}

		if foundCountry == true {
			if len(record[0]) > 0 {
				break
			}
		}

		if record[0] == countryName {
			foundCountry = true
		}

		if foundCountry == true {
			s = append(s, record[1])
		}
	}
	return s, nil
}

func getServers(cityName string) ([]string, error) {
	var s []string

	r := csv.NewReader(bytes.NewReader(serverData))

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("error whilst parsing list of servers from %s. Error: %s", cityName, err)
		}
		if record[1] == cityName {
			return strings.Fields(record[2]), nil
		}
	}
	return s, nil

}
