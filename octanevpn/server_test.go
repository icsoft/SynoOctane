// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

package octanevpn_test

import (
	"testing"

	"github.com/icsoft/SynoOctane/octanevpn"
)

func TestGetCountries(t *testing.T) {
	retval, err := octanevpn.GetCountries()
	if err != nil {
		t.Error(err)
	}
	if retval[0] != "Australia" {
		t.Error("Invalid country data return, Expected Australia, got ", retval)
	}
}

func TestGetCities(t *testing.T) {
	retval, err := octanevpn.GetCities("Australia")
	if err != nil {
		t.Error(err)
	}
	if retval[0] != "Brisbane" {
		t.Error("Invalid city data return, Expected Brisbane, got ", retval)
	}
}

func TestGetServers(t *testing.T) {
	retval, err := octanevpn.GetServers("Brisbane")
	if err != nil {
		t.Error(err)
	}
	if retval[0] != "gw2.bne1.octanevpn.com" {
		t.Error("Invalid server data return, Expected gw2.bne1.octanevpn.com, got ", retval)
	}
}
