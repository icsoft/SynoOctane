// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltdz

package octanevpn

const octaneVPNServers = `Australia,Brisbane,gw2.bne1.octanevpn.com
,Melbourne,gw3.mel1.octanevpn.com
,Perth,gw1.per2.octanevpn.com gw3.per2.octanevpn.com 
,Sydney,gw2.syd1.octanevpn.com
Austria,Graz,gw1.grz1.octanevpn.com
Belgium,Ostend,gw1.ost2.octanevpn.com gw1.ost1.octanevpn.com 
Brazil,Sao Paulo,gw1.gru1.octanevpn.com
Bulgaria,Sofia,gw1.sof1.octanevpn.com
Canada,Montreal,gw1.yul1.octanevpn.com gw1.yul2.octanevpn.com 
,Québec City,gw1.yqb1.octanevpn.com
,Richmond,gw1.yvr1.octanevpn.com
,Toronto,gw1.yyz1.octanevpn.com gw3.yyz1.octanevpn.com gw1.yyz2.octanevpn.com 
Chile,Vina del Mar,gw1.kna1.octanevpn.com
China,Tianjin,gw1.tsn1.octanevpn.com
Czech Republic,Prague,gw1.prg1.octanevpn.com
Denmark,Copenhagen,gw1.cph1.octanevpn.com
France,Lille,gw1.lil1.octanevpn.com
,Paris,gw1.cdg1.octanevpn.com gw1.cdg2.octanevpn.com 
,Strasbourg,gw1.sxb1.octanevpn.com
Germany,Dusseldorf,gw2.dus2.octanevpn.com gw1.dus1.octanevpn.com 
,Frankfurt,gw1.fra2.octanevpn.com gw1.fra3.octanevpn.com gw1.fra1.octanevpn.com 
,Nürnberg,gw1.nue1.octanevpn.com
Hong Kong,Hong Kong,gw1.hkg1.octanevpn.com gw1.hkg2.octanevpn.com 
Hungary,Budapest,gw1.bud1.octanevpn.com
Iceland,Reykjavik,gw1.rkv1.octanevpn.com
India,Chennai/Madras,gw1.maa1.octanevpn.com
,Pune,gw3.pnq1.octanevpn.com gw1.pnq1.octanevpn.com 
Indonesia,Jakarta,gw1.cgk1.octanevpn.com
Ireland,Dublin,gw1.dub2.octanevpn.com gw1.dub1.octanevpn.com 
Israel,Tel Aviv Yafo,gw1.tlv2.octanevpn.com gw1.tlv1.octanevpn.com 
Italy,Milan,gw1.mxp1.octanevpn.com gw1.mxp2.octanevpn.com 
Japan,Tokyo,gw1.nrt2.octanevpn.com gw1.nrt1.octanevpn.com 
Latvia,Riga,gw1.rix1.octanevpn.com
Luxembourg,Luxembourg,gw1.lux1.octanevpn.com
Macau,Macau,gw1.mfm1.octanevpn.com
Malaysia,Kuala Lumpur,gw1.kul3.octanevpn.com gw1.kul2.octanevpn.com 
Netherlands,Amsterdam,gw1.ams2.octanevpn.com gw1.ams1.octanevpn.com gw1.ams4.octanevpn.com 
New Zealand,Auckland,gw1.akl1.octanevpn.com
Norway,Torp,gw1.trf1.octanevpn.com
Panama,Panama City,gw1.pty1.octanevpn.com
Poland,Gdansk,gw1.gdn2.octanevpn.com gw1.gdn1.octanevpn.com 
,Warsaw,gw1.waw1.octanevpn.com
Portugal,Lisbon,gw1.lis1.octanevpn.com
Romania,Bucharest,gw1.buh2.octanevpn.com
Russian Federation,Moscow,gw1.svo1.octanevpn.com gw1.svo2.octanevpn.com 
Singapore,Singapore,gw1.sin2.octanevpn.com gw1.sin1.octanevpn.com 
South Africa,Cape Town,gw1.cpt1.octanevpn.com
,Lanseria,gw1.hla1.octanevpn.com
Spain,Barcelona,gw1.bcn2.octanevpn.com
,Madrid,gw1.mad1.octanevpn.com
Sweden,Halmstad,gw1.had2.octanevpn.com
,Stockholm,gw1.arn2.octanevpn.com gw1.arn3.octanevpn.com 
Switzerland,Zurich,gw1.zrh2.octanevpn.com gw1.zrh1.octanevpn.com 
Taiwan,Taichung,gw1.rmq1.octanevpn.com
,Taipei,gw1.tpe1.octanevpn.com
Thailand,Bangkok,gw1.bkk1.octanevpn.com
Turkey,Istanbul,gw1.ist1.octanevpn.com
Ukraine,Kiev,gw1.iev1.octanevpn.com
United Kingdom,Coventry,gw1.cvt1.octanevpn.com
,Eastleigh near Southampton,gw2.sou1.octanevpn.com
,Isle Of Man,gw1.iom1.octanevpn.com
,London,gw2.lhr2.octanevpn.com gw2.lhr1.octanevpn.com gw1.lhr3.octanevpn.com gw3.lhr1.octanevpn.com gw1.lhr4.octanevpn.com gw1.lhr2.octanevpn.com gw1.lhr1.octanevpn.com 
,Manchester,gw1.man2.octanevpn.com
,Rochester,gw2.rcs1.octanevpn.com
United States,Atlanta,gw1.atl1.octanevpn.com gw2.atl2.octanevpn.com gw1.atl3.octanevpn.com 
,Boston,gw1.bos1.octanevpn.com
,Buffalo NY,gw1.buf1.octanevpn.com
,Charlotte,gw1.clt1.octanevpn.com
,Chicago,gw1.ord3.octanevpn.com gw1.ord4.octanevpn.com gw2.ord1.octanevpn.com gw1.ord1.octanevpn.com 
,Columbus,gw1.cmh1.octanevpn.com
,Dallas,gw1.dfw1.octanevpn.com gw1.dfw3.octanevpn.com gw1.dfw2.octanevpn.com gw1.dfw4.octanevpn.com gw2.dfw3.octanevpn.com 
,Denver,gw1.den1.octanevpn.com gw1.den2.octanevpn.com 
,Dubuque,gw1.dbq1.octanevpn.com
,Fletcher,gw1.avl2.octanevpn.com
,Houston,gw1.hou1.octanevpn.com
,Kansas City,gw1.mci2.octanevpn.com gw2.mci2.octanevpn.com gw2.mci1.octanevpn.com 
,Las Vegas,gw1.las1.octanevpn.com
,Los Angeles,gw1.lax2.octanevpn.com gw1.lax1.octanevpn.com gw1.lax4.octanevpn.com gw1.lax3.octanevpn.com 
,Miami,gw1.mia3.octanevpn.com gw1.mia4.octanevpn.com 
,Morganton,gw1.mrn1.octanevpn.com
,New York,gw1.lga2.octanevpn.com gw1.lga1.octanevpn.com 
,Newark,gw2.ewr1.octanevpn.com gw1.ewr1.octanevpn.com 
,Orlando,gw1.mco2.octanevpn.com
,Palo Alto,gw2.pao1.octanevpn.com gw1.pao1.octanevpn.com 
,Phoenix,gw1.phx2.octanevpn.com gw1.phx1.octanevpn.com 
,Roseburg,gw1.rbg1.octanevpn.com
,Salt Lake City UT,gw1.slc2.octanevpn.com gw1.slc1.octanevpn.com gw2.slc1.octanevpn.com 
,San Diego,gw1.san1.octanevpn.com
,San Jose,gw1.sjc2.octanevpn.com gw1.sjc1.octanevpn.com 
,Seattle WA,gw1.sea3.octanevpn.com gw2.sea1.octanevpn.com gw1.sea2.octanevpn.com 
,St Louis,gw1.stl1.octanevpn.com
,Tampa,gw1.tpa1.octanevpn.com
,Washington DC,gw1.iad1.octanevpn.com gw2.iad1.octanevpn.com gw1.iad2.octanevpn.com`
