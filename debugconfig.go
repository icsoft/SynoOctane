// +build debug

package main

// Redirect the configuration directory for debugging purposes.
// Use -tags 'debug' to enable this when using go build.
func init() {
	configPath = "." //Debug config path.
}
