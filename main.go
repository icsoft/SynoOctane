// SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd

// This utility will adjust the standard Synology VPN client to work with
// Octane VPN as per their config file.
// You can also optionally change the Octane VPN server that is used.
package main

import (
	"flag"
	"fmt"
	"math"
	"os"
	"strconv"
	"time"

	"github.com/icsoft/SynoOctane/net"
	"github.com/icsoft/SynoOctane/octanevpn"
	"github.com/icsoft/SynoOctane/synolog"
	"github.com/icsoft/SynoOctane/synovpn"
)

// Standard Synology config path. This path is redirected in debugconfig.go
// for debugging.
var configPath = "/usr/syno/etc/synovpnclient/openvpn"
var vpn synovpn.VPNService

type newSvrType int

const (
	initial newSvrType = iota
	random
	chosen
)

const version = "SynoOctane V0.3, April 2016"
const licence = `
SynoOctane  Copyright (C) 2016  Icsoft Pty Ltd
This program comes with ABSOLUTELY NO WARRANTY.
This is free software, and you are welcome to redistribute it
under certain conditions; type [L] for details.`

const options = `
SynoOctane main menu, please make your selection:
[I] Initial setup
[C] Change Octane VPN server
[R] Random Octane VPN server
[E] get External IP address
[N] get Network IP address(es)
[X] eXit
`
const licenceFull = `
Copyright (c) 2016 Icsoft Pty Ltd, Australia (www.icsoft.com.au)

SynoOctane is free software: you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

SynoOctane is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with SynoOctane.  If not, see <http://www.gnu.org/licenses/>.`

func main() {
	//setup the logger
	synolog.SynoLog(synolog.LogInformation, "SynoOctane started")

	// Handle command line args
	if len(os.Args[1:]) > 0 {
		ipHaz := flag.Bool("iphaz", false, "Get External IP address from icanhazip.com")
		ipDyn := flag.Bool("ipdyn", false, "Get External IP address from dyndns.org")
		rnd := flag.Bool("rnd", false, "Change to random OctaneVPN server")
		ver := flag.Bool("v", false, "Display version information")
		flag.Parse()
		if *ipHaz {
			fmt.Print(net.ExternalHazIP())
		}
		if *ipDyn {
			fmt.Print(net.ExternalDynIP())
		}
		if *rnd {
			writeNewConfig(random)
		}
		if *ver {
			fmt.Println(version)
		}
		os.Exit(0)
	}

	// Check if we are in debug mode as the path changes
	if configPath == "." {
		fmt.Print("\n\nWARNING THIS APPLICATION IS USING THE DEBUG FILE PATH\nDo you want to continue?\nEnter [Y] to continue?\n\n")
		var input string
		fmt.Scanln(&input)
		if !(input[0] == 'y' || input[0] == 'Y') {
			os.Exit(0)
		}
	}

	fmt.Printf("\n%s\n", licence)

menu:
	for {
		fmt.Printf("\n%s\n", options)

		var input string
		fmt.Scanln(&input)

		switch input[0] {
		case 'i', 'I':
			writeNewConfig(initial)
		case 'c', 'C':
			writeNewConfig(chosen)
		case 'r', 'R':
			writeNewConfig(random)
		case 'l', 'L':
			fmt.Println(licenceFull)
			time.Sleep(5 * time.Second)
			continue menu
		case 'x', 'X':
			fmt.Println("User cancelled")
		case 'e', 'E':
			fmt.Println("checking dyndns.org . . .")
			ip1, err := net.ExternalDynIP()
			if err != nil {
				fmt.Printf("unable to get IP from DynDns, error: %s\n", err)
			}
			fmt.Println(ip1)
			fmt.Println("checking icanhazip.com . . .")
			ip2, err := net.ExternalHazIP()
			if err != nil {
				fmt.Printf("unable to get IP from ICanHaz, error: %s\n", err)
			}
			fmt.Println(ip2)
			continue menu
		case 'n', 'N':
			ips, err := net.NetworkIP()
			if err != nil {
				fmt.Printf("Unable to get local interfaces network addresse. Error returned: %s", err)
			}
			fmt.Println(ips)
			continue menu
		default:
			fmt.Printf("Valid selections are 'i,I,c,C,r,R,l,L,e,E,x,X' whereas you typed %s.\nPlease enter a valid character next time\n", string(input[0]))
			fmt.Printf("For valid command line arguments, use the command line help (%s -h)", os.Args[0])
			time.Sleep(2 * time.Second)
			continue menu
		}
		break
	}

	fmt.Println("\nThank you for using SynoOctane.")
	os.Exit(0)
}

func writeNewConfig(svrType newSvrType) {

	synConf, err := synovpn.GetSynoVPNConfig(configPath)
	if err != nil {
		fmt.Printf("Unable to retrieve the synology configuration, Error returned: %s\n", err)
		os.Exit(1)
	}

	var svr string
	switch svrType {
	case initial:
		svr = synConf.Server
	case random:
		newServer, err := octanevpn.GetRandomServer()
		if err != nil {
			fmt.Printf("Unable to retrieve random server, Error: %s\n", err)
			os.Exit(1)
		}
		svr = newServer
	case chosen:
		newServer, err := getNewServer()
		if err != nil {
			fmt.Printf("Unable to get new server, error returned: %s", err)
			os.Exit(1)
		}
		svr = newServer
	}

	s, err := synovpn.WriteConfig(svr, synConf.ClientID, configPath)
	if err != nil {
		fmt.Printf("Unable to write to VPN config, error returned: %s\n", err)
		os.Exit(1)
	}
	fmt.Println(s)
}

// getNewServer uses selectInput to enable the user to select a new OctaneVPN
// server from the list then returns the selected server.
func getNewServer() (string, error) {
	var od = octanevpn.NewOctaneData()
	var filter string

	for x := octanevpn.Country; x <= octanevpn.Server; x++ {
		od.SetFilterType(x)
		data, err := od.Data()
		if err != nil {
			return "", err
		}
		filter, err := selectInput(x.String(), data)
		if err != nil {
			return "", err
		}
		od.SetFilter(filter)
	}
	return filter, nil
}

// selectInput is the command line interface for user OctaneVPN server selection
func selectInput(selectionType string, selectionList []string) (string, error) {
	if len(selectionList) == 0 {
		return "", fmt.Errorf("No OctaneVPN server list found")
	}

	if len(selectionList) == 1 {
		fmt.Printf("%s: %s\n\n\n", selectionType, selectionList[0])
		return selectionList[0], nil
	}

	fmt.Printf("Octane VPN %s selection list\n\n", selectionType)

	var i int
	for i = 0; i < len(selectionList); i++ {
		fmt.Printf("[%d] %s\t\t", i, selectionList[i])
		if math.Mod(float64(i), 2) != 0 {
			fmt.Println()
		}
	}

	fmt.Println()
	fmt.Printf("[%d] to cancel\n\n", i)

	fmt.Printf("Enter required %s or %s number: ", selectionType, selectionType)
	var input string
	fmt.Scanln(&input)

	s, err := strconv.ParseInt(input, 10, 32)
	if err == nil {
		if s == int64(i) {
			os.Exit(1)
		}
		input = selectionList[s]
	}

	fmt.Printf("%s: %s chosen\n\n\n", selectionType, input)
	time.Sleep(2 * time.Second)
	return input, nil
}

func synoVersion() {
	// /etc.defaults/VERSION
	// majorversion="6"
	// minorversion="0"
	// productversion="6.0"
	// buildphase="release"
	// buildnumber="7321"
	// smallfixnumber="3"
	// builddate="2016/04/21"
	// buildtime="11:48:23"

}
