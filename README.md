<!-- Welcome to SynoOctane -->

# SynoOctane 
[![Build Status](https://travis-ci.org/icsoft/SynoOctane.svg?branch=master)](https://travis-ci.org/icsoft/SynoOctane)
[![GoDoc](https://godoc.org/github.com/icsoft/SynoOctane?status.png)](https://godoc.org/github.com/icsoft/SynoOctane)
[![Go Report Card](https://goreportcard.com/badge/github.com/icsoft/SynoOctane)](https://goreportcard.com/report/github.com/icsoft/SynoOctane)


NOTE: Redundant utility
-----------------------
Thankfully this utility has been made redundant by Synology DSM V6 (or greater), unfortunately if you cannot get DSM 6 (2010 or earlier) you are stuck with this.  
You can still use this utility to show external IP address, or add to a script etc. 


Octane VPN server selection for Synology NAS
--------------------------------------------

This utility will adjust the standard Synology VPN client to work with Octane VPN as per their config file.
You can also optionally change the Octane VPN server that is used.
SynoOctane was written as it is easier to use than trying to get 'sed' to work correctly and it makes it considerably easy to change server if you need to change IP address or when a a server stops working. 

### SynoOctane is neither endorced by, nor is an official product of, Synology Inc. or Octane Networks LLC, the use of their company names here is purely descriptive of the functionality of this product.

Synology Inc. and Octane Networks LLC official sites can be found at the links below.
* Octane VPN: <https://www.octanevpn.com/>
* Synology: <http://www.synology.com/>

The synology VPN client is found in `/usr/syno/etc/synovpnclient/openvpn`

Octane VPN client certificate: <http://www.octanevpn.com/reseller/index/crtfile/OctaneVPN.crt>

How it works
------------
After you have entered the minimum information as required in the Synology web based UI run SynoOctane and it will change the Synology VPN client configuration file to work with Octane VPN.
SynoOctane uses the Synology ovpnclient.conf to add the OCTANESERVER you chose in the NAS UI and the VPNCLIENTID to point the configuration file to the correct certificate, then writes that to an new coonfiguration file.

### NOTE: you will need the ARM build if your NAS is based on ARM or the X86 build if it is Intel/AMD based 

How to use it
-------------
### First time setup of your NAS with Octane VPN
* Follow the Synology UI instructions from Octane VPN <http://www.octanevpn.com/tutorials/openvpn-on-synology.html> and stop at "6. Login to the Synology as root User".
* Log into your NAS as the root user.
* Download the appropriate SynoOctane app for your NAS:
	* All releases found at `https://github.com/icsoft/SynoOctane/releases`.
    * ARM executable `wget https://github.com/icsoft/SynoOctane/releases/download/v0.2-alpha/SynoOctane_v0.2_arm5tel`.
	* X86 executable `wget https://github.com/icsoft/SynoOctane/releases/download/v0.2-alpha/SynoOctane_v0.2_x86_64`.
* Change file permissions to executable `chmod 700 SynoOctane_v0.2_arm5tel`.
* Run SynoOctane and select "I" for Initial Setup './SynoOctane_v0.2_arm5tel'.
* Go back to the Synology UI and start your VPN connection.

### Subsequent uses to change Octane VPN servers
* Log into your NAS as the root user.
* Run SynoOctane and select "C" to Change the Octane VPN server './SynoOctane_v0.2_arm5tel'.
* Go back to the Synology UI and start your VPN connection.


Configuration files
-------------------

The client config needs Octane VPN specific configuration as below where:
* OCTANESERVER is the canonical domain name of the VPN server you would like to connect to. and
* VPNCLIENTID is the numerical identifier that the synology NAS associates to the VPN configuration file.
* OCTANEVPNUSERNAME the username supplied to you by Octane VPN

```
client
cipher AES-256-CBC
remote-cert-tls server
keepalive 10 120
nobind
persist-key
persist-tun
log vpnclient.log
remote OCTANESERVER 443
dev tun
proto udp
ca ca_oVPNCLIENTID.crt
redirect-gateway
script-security 2
float
reneg-sec 0
explicit-exit-notify
plugin /lib/openvpn/openvpn-down-root.so /usr/syno/etc.defaults/synovpnclient/scripts/ip-down
auth-user-pass /tmp/ovpn_client_up
```

Expected Synology ovpnclient.conf
(To view this type `cat /usr/syno/etc/synovpnclient/openvpn/ovpnclient.conf`)

```

[oVPNCLIENTID]
nat=no
protocol=udp
redirect-gateway=yes
comp-lzo=no
pass=PASSWORD
port=443
reconnect=yes
conf_name=OctaneVPN
user=OCTANEVPNUSERNAME@octanevpn
remote=OCTANESERVER
```

Licence: LGPL
------------
    Copyright (c) 2016 Icsoft Pty Ltd, Australia

    SynoOctane is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    SynoOctane is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with SynoOctane.  If not, see <http://www.gnu.org/licenses/>.

Addendum
----------
Some of the code in this utility is unnecessary, and added mostly as a learning tool (eg. build tags).
This utility will be updated if Octane VPN allows for an easy way to get their server list, rather than having to rip it out of their web page.

Currently tested on
-------------------
### Synology OS 
* 5.0
* 5.1
* 5.2

### Models
* DS415+ - X86_64
* DS210j - arm5tel